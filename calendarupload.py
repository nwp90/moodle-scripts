#!/usr/bin/env python3
""" Moodle calendar uploader """

import argparse
import csv
import json
import os
import datetime
import pytz
import html
import requests
import http.client as http_client
import logging
import pprint
import sys
from functools import reduce
from itertools import chain

import muddle
#from muddle.utils import clean_username

level = 'CRITICAL'
logging.basicConfig()
log = logging.getLogger()
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.propagate = True
# levels set here used only until config has been read
log.setLevel(level)
requests_log.setLevel(level)

FORMAT_MOODLE = 0
FORMAT_HTML = 1
FORMAT_PLAIN = 2
FORMAT_MARKDOWN = 3

USERS_AS_GROUPS = True
CREATE_USERGROUPS = False

alternative_fields = {
    'Title': 'Event_Name',
    'Duration-hours': 'Duration_minutes',
    'ModuleLink_Text': 'ModuleLink_Descr',
    'Location-Shortname': 'Location',
    'Location_Shortname': 'Location'
}

required_fields = set([
    'Event_Name',
    'Event_Type-Shortname',
    'Student-Group_Name',
    'Date',
    'Start_Time',
    'Duration_minutes',
    'Activity_URL',
    'Activity_Name',
    'ModuleLink_Shortname',
    'ModuleUpload_Shortname',
    'Staff',
    'Location',
    'Dept-Contact_Details',
    'Comments'
])

optional_fields = set([
    'ModuleLink_Descr'
])

class Configuration(object):
    """ Class to handle creation and use of configuration from cli flags,
        JSON ini file, and defaults
    """
    defaults = {
        'debug': False,
        'config': os.path.join(os.path.expanduser('~'), '.mdl'),
        'input': None,
        'service': None,
        'services': {},
        'httploglevel': None,
        'loglevel': None,
        'requestslevel': None,
        }

    def __init__(self):
        argparser = argparse.ArgumentParser(description='Add calendar events to Moodle based on supplied CSV data')
        argparser.add_argument('-c', '--config', default=self.defaults['config'], help='path to JSON config file (default is ~/.mdl)')
        argparser.add_argument('-s', '--service', default=None, help='name of service to access (available services defined in config file)')
        argparser.add_argument('-i', '--input', default=None, help='name of input data file, "-" for stdin')
        argparser.add_argument('-n', '--dryrun', action='store_true', default=False, help='dry run - check that input is valid and report on errors but do not commit')
        argparser.add_argument('-d', '--debug', action='store_true', default=None, help='debug mode')
        self.argparser = argparser
        self.args = argparser.parse_args()

    @classmethod
    def get(cls):
        if getattr(cls, '__config', None) is None:
            cls.__config = cls()
        return cls.__config

    def usage(self):
        self.argparser.print_help()
        return

    def error(self, msg):
        sys.stderr.write(u'ERROR: %s\n' % msg)
        self.argparser.print_help()
        sys.exit(1)

    def get_item(self, name):
        if name != 'config':
            if getattr(self, 'conf', None) is None:
                self.read_json_config()
            conf = self.conf
        else:
            conf = {}
        # global config options
        if name in ['sites', 'services']:
            key = name
        else:
            key = 'default_' + name
        var = conf.get(key, None)
        if var is None:
            var = self.defaults.get(name, None)
        if getattr(self.args, name, None) is not None:
            var = getattr(self.args, name, None)
        return var

    def read_json_config(self):
        conffile = open(self.get_item('config'))
        self.conf = json.load(conffile)
        conffile.close()
        return self.conf

    def merge_site_config(self):
        site_config = self.get_site()

    def get_site(self):
        site = self.get_item('site')
        sites = self.get_item('sites')
        if site is None:
            self.error(u'No site specified')
        site_config = sites.get(site, None)
        if site_config is None:
            self.error(u"No config available for site '%s'" % site)
        return site_config

    def get_service(self):
        service = self.get_item('service')
        services = self.get_item('services')
        if service is None:
            self.error(u'No service specified')
        service_config = services.get(service, None)
        if service_config is None:
            self.error(u"No config available for service '%s'" % service)
        return service_config

def read_json_config():
    conffile = open(os.path.join(os.path.expanduser('~'), '.mdl'))
    conf = json.load(conffile)
    conffile.close()
    return conf

conf = Configuration.get()
httplevel = 0
requestslevel = None
debug = conf.get_item('debug')
if debug:
    level = 'DEBUG'
    requestslevel = 'DEBUG'
    log.setLevel(level)
    requests_log.setLevel(level)
    httplevel = 0

if conf.get_item('httploglevel') is not None:
    httplevel = conf.get_item('httploglevel')

if httplevel is not None:
    http_client.HTTPConnection.debuglevel = int(httplevel)

if conf.get_item('loglevel') is not None:
    level = conf.get_item('loglevel')
if level is not None:
    log.setLevel(level)
    requests_log.setLevel(level)

if conf.get_item('requestsloglevel') is not None:
    requestslevel = conf.get_item('requestsloglevel')
if requestslevel is not None:
    requests_log.setLevel(requestslevel)

log.info("Debug: %s" % debug)
log.info("HTTP log level (numeric): %s" % httplevel)
log.info("Requests log level (named): %s" % requestslevel)
log.info("General log level (named): %s" % level)

log.warning("Using service '%s'" % conf.get_item('service'))
service = conf.get_service()
moodlebase = service['baseurl']
moodletoken = service['token']

moodlews = '%s/webservice/restjson/server.php' % moodlebase

infile = conf.get_item('input')
if infile is None:
    conf.usage()
    sys.exit(1)

m = muddle.Config(moodletoken, moodlebase)

mcatapi = muddle.category.API(m)
mcrsapi = muddle.course.API(m)
mgrpapi = muddle.group.API(m)
musrapi = muddle.users.API(m)

eventlist = []
events = { 'events': [] }
errors = []
warnings = []
# k: shortname, v: id
modules = {}

class Event(object):
    """ A calendar event """

    tolookup = {
        'mods': set(),
        'groups': {},
        'users': set()
    }
    modulelookup = {}
    grouplookup = {}
    userlookup = {}
    owner = {
        'type': None,
        'name': None,
        'id': None,
    }
    targetmodule = {
        'name': None,
        'id': None,
    }
    linkmodule = {
        'name': None,
        'text': None,
    }
    startdt = None
    duration = None
    title = None
    location = {
        'short': None,
        'full': None,
    }
    comments = None
    staff = None
    contactdept = None
    activity = {
        'name': None,
        'url': None,
    }
    eventtype = None
    topic = None

    def __init__(self):
        self.errors = []
        self.warnings = []
        pass

    def _find_title(self, row):
        """ Find event title in spreadsheet row and set self.title """
        self.title = html.escape(row.get('Title', row.get('Event_Name', 'Unnamed event')))
        return

    def _find_usereventtype(self, row):
        """ Find event type in spreadsheet row and set self.usereventtype """
        self.usereventtype = html.escape(row.get('Event_Type-Shortname', '???'))
        return

    def _find_owner(self, row):
        """ Find event owner in spreadsheet row, add to list of users to look up,
            if we have a target module then add to list of groups to look for in
            that module.
        """
        self.owner = { 'name': row.get('Student-Group_Name', None) }
        self.tolookup['users'] |= set([self.owner['name']])
        modname = self.targetmodule.get('name', None)
        if modname is not None:
            modgroups = self.tolookup['groups'].get(modname, set())
            self.tolookup['groups'][modname] = modgroups | set([self.owner['name']])
        return

    def _find_start(self, row):
        """ Find event start datetime in spreadsheet row and set self.startdt.
            Warn if far into future or in past.
        """
        # We seem to get dd/mm/yy
        startdate = row.get('Date', None)
        if startdate is None:
            self.errors.append('No start date')
            return
        day, month, year = startdate.split('/', 2)
        if len(year) == 2:
            year = '20%s' % year
        starttime = row.get('Start_Time', None)
        if starttime is None:
            self.errors.append('No start time')
            return
        timeparts = starttime.split(':', 2)
        hour = timeparts.pop(0)
        minute = timeparts.pop(0)
        if timeparts:
            second = timeparts.pop(0)
        else:
            second = 0
        startdatetime = datetime.datetime(year=int(year), month=int(month), day=int(day),
                                          hour=int(hour), minute=int(minute), second=int(second),
                                          tzinfo=None)
        # .timestamp does localisation anyway, but explicit is good.
        self.startdt = pytz.timezone('Pacific/Auckland').localize(startdatetime).timestamp()
        if startdatetime - datetime.datetime.now() > datetime.timedelta(days=500):
            self.warnings.append("Event start is a long way into the future (%s %s)" % (starttime, startdate))
        if startdatetime < datetime.datetime.now():
            self.warnings.append("Event start is in the past (%s %s)" % (starttime, startdate))
        return

    def _find_duration(self, row):
        """ Find duration in spreadsheet row, convert to minutes if
            necessary, and set self.duration.
        """
        # Consistency of naming would be nice...
        dminutes = row.get('Duration_minutes', None)
        if dminutes is None:
            dminutes = float(row.get('Duration-hours', 0)) * 60
        self.duration = int(float(dminutes) * 60)
        return

    def _find_activity(self, row):
        """ Find activity name & URL in spreadsheet row, and set up self.activity """
        activityurl = row.get('Activity_URL', None)
        if activityurl is not None:
            activityurl = html.escape(activityurl, quote=True)
        activityname = html.escape(row.get('Activity_Name', ''))
        self.activity = {
            'url': activityurl,
            'name': activityname,
            'link': None,
        }
        self.get_activity_link()
        return

    def _find_linkmodule(self, row):
        """ Find module to link to in spreadsheet row, and set up self.linkmodule.
            Also add module to list of modules to look up.
        """
        modshortname = row.get('ModuleLink_Shortname', None)
        modtext = row.get('ModuleLink_Descr', row.get('ModuleLink_Text', modshortname))
        if modtext is not None:
            modtext = html.escape(modtext)
        # Must not stringify None in module shortname, or things get confusing later
        if modshortname is not None:
            modshortname = html.escape(modshortname)
        self.linkmodule = {
            'name': modshortname,
            'text': modtext,
        }
        self.tolookup['mods'] |= set([modshortname])
        return

    def _find_targetmodule(self, row):
        """ Find the module we're uploading event to in spreadsheet row, add
            to modules to look up, and set targetmodule for this event.
            Flag an error if targetmodule is missing from row.
        """
        modshortname = row.get('ModuleUpload_Shortname', None)
        if modshortname is not None:
            modshortname = html.escape(modshortname)
            self.tolookup['mods'] |= set([modshortname])
            self.targetmodule = {
                'name': modshortname,
                'id': None,
            }
        else:
            self.errors.append("Missing ModuleUpload_Shortname")
        return

    def _find_staff(self, row):
        """ Find staff in spreadsheet row and set self.staff """
        self.staff = html.escape(row.get('Staff', 'Unnamed staff'))
        return

    def _find_location(self, row):
        """ Find descriptions of location available in spreadsheet row, and
            set up self.location.
        """
        fullloc = row.get('Location', None)
        shortloc = row.get('Location-Shortname', row.get('Location_Shortname', None))
        if fullloc is None and shortloc is None:
            self.errors.append('Event with neither "Location" nor "Location-Shortname"')
        else:
            if fullloc is None:
                fullloc = 'Unspecified location'
        if fullloc is not None:
            fullloc = html.escape(fullloc)
        if shortloc is not None:
            shortloc = html.escape(shortloc)
        self.location = {
            'full': fullloc,
            'short': shortloc
        }
        return

    def _find_contactdept(self, row):
        """ Find department contact in spreadsheet row and set self.contactdept """
        self.contactdept = html.escape(row.get('Dept-Contact_Details', 'Contact details not supplied'))
        return

    def _find_comments(self, row):
        """ Find comments in spreadsheet row and set self.comments """
        self.comments = html.escape(row.get('Comments', ''))
        return

    def lookup_module(self, modulename):
        """ lookup and cache module id using Moodle WS """
        response = mcrsapi.get_courses_by_field('shortname', modulename)
        if 'errorcode' in response:
            self.errors.append('lookup_module got error: %s' % response['message'])
            return None
        modlist = response.get('courses', [])
        for mod in modlist:
            if mod['shortname'] == modulename:
                modid = mod.get('id', None)
                self.modulelookup[modulename] = modid
                return modid
        return None

    def lookup_group(self, modulename, groupname):
        """ lookup and cache id of module group 'groupname' for module with id 'moduleid' using Moodle WS """
        #print("lookup_group looking for group '%s' in module '%s'..." % (groupname, modulename))
        moduleid = self.get_module_id(modulename)
        if moduleid is None:
            self.errors.append("can't lookup group for invalid module: %s" % modulename)
            return None
        #print("lookup_group using module id %s..." % moduleid)
        grouplist = mgrpapi.get_course_groups(moduleid)
        if 'errorcode' in grouplist:
            #print('got error: %s' % pprint.pformat(grouplist))
            self.errors.append('lookup_group got error: %s' % grouplist['message'])
            return None
        if grouplist:
            for group in grouplist:
                if group['name'] == groupname:
                    if not(modulename in self.grouplookup):
                        self.grouplookup[modulename] = {}
                    self.grouplookup[modulename][groupname] = group['id']
                    return group['id']
        #print("lookup_group failed to find %s for module '%s'" % (groupname, modulename))
        return None

    def lookup_user(self, username):
        """ lookup and cache id of user with supplied username using Moodle WS """
        log.debug("lookup_user looking for user '%s'..." % username)
        userlist = musrapi.get_users_by_field('username', [username])
        #pprint.pprint(userlist)
        if 'errorcode' in userlist:
            self.errors.append('lookup_user got error: %s' % userlist['message'])
            return None
        if userlist:
            for user in userlist:
                if user['username'] == username:
                    self.userlookup[username] = user['id']
                    return user['id']
        log.debug("lookup_user failed to find user '%s'" % username)
        return None

    @classmethod
    def lookup_modules(cls):
        """ lookup module ids for all modules for which an entry exists in tolookup['mods'] """
        errors = []
        log.debug("Looking up modules: %s" % pprint.pformat(cls.tolookup['mods']))
        for modname in cls.tolookup['mods']:
            response = mcrsapi.get_courses_by_field('shortname', modname)
            if 'errorcode' in response:
                errors.append('lookup_modules got error for module "%s": %s' % (modname, response['message']))
                continue
            modmap = dict([(c['shortname'], c) for c in response.get('courses', [])])
            if modname in modmap:
                cls.modulelookup[modname] = modmap[modname]['id']
            else:
                cls.modulelookup[modname] = None
        return errors

    @classmethod
    def lookup_users(cls):
        """ lookup user ids for all users for which an entry exists in tolookup['users'] """
        errors = []
        log.debug("lookup_users looking for users:\n%s" % pprint.pformat(cls.tolookup['users']))
        userlist = musrapi.get_users_by_field('username', cls.tolookup['users'])
        #pprint.pprint(userlist)
        if 'errorcode' in userlist:
            errors.append('lookup_users got error: %s' % userlist['message'])
            return errors
        usermap = dict([(u['username'], u) for u in userlist])
        for username in cls.tolookup['users']:
            user = usermap.get(username, None)
            userid = user['id'] if user is not None else None
            cls.userlookup[username] = userid
        return errors

    def get_activity_link(self):
        """ Get HTML link to activity, if possible, based on activity name and URL.
            If no URL, use plain text of name instead.
        """
        if self.activity.get('link', None) is None:
            activityurl = self.activity.get('url', None)
            activityname = self.activity.get('name', activityurl)
            if not(activityurl in ('', None)):
                return '<a href="%s">%s</a>' % (activityurl, activityname)
            else:
                return activityname

    def get_module_link(self):
        """ Get HTML link to module for event """
        modname = self.linkmodule.get('name', None)
        if not('id' in self.linkmodule):
            self.linkmodule['id'] = self.get_module_id(modname)
        if self.linkmodule.get('id', None) is None:
            moduleurl = "/course"
        else:
            moduleurl = "/course/view.php?id=%(id)s" % self.linkmodule
        linkname = self.linkmodule.get('text', None)
        if linkname is None:
            linkname = 'Unspecified module'
        return '<a href="%s">%s</a>' % (moduleurl, linkname)

    def get_targetmodule_id(self):
        """ Get target module id if event is of type that has target module
            (i.e. course or group event), otherwise return None.
        """
        # what about getting SITEID if type is "site"?
        if not self.get_ownertype() in ('course', 'group'):
            return None
        return self._target_moduleid()

    def _target_moduleid(self):
        """ Get target module id if appropriate event type, and set owner id
            to module id if course event.
        """
        name = self.targetmodule.get('name', None)
        eventtype = self.owner.get('type', None)
        if eventtype in ('user', 'site'):
            return None
        targetid = self.get_module_id(name)
        if eventtype == 'course':
            self.owner['id'] = targetid
        return targetid

    def get_module_id(self, modulename):
        """ Get id for module with given name """
        if modulename is None:
            return None
        if not(modulename in self.modulelookup):
            self.modulelookup[modulename] = self.lookup_module(modulename)
        if self.modulelookup[modulename] is None:
            self.errors.append('Invalid module name "%s" gives id None' % modulename)
        #print("get_module_id returning: %s" % self.modulelookup[modulename])
        return self.modulelookup[modulename]

    def get_targetgroup_id(self):
        """ Get target group id, or None if not a group event. """
        if self.get_ownertype() != 'group':
            return None
        return self._target_groupid()

    def _target_groupid(self):
        """ Get target group id by lookup from name. Set event owner id if appropriate. """
        gname = self.owner.get('name', None)
        if self.owner.get('type', None) in ('user', 'course') or gname is None:
            return None
        if self.owner.get('id', None) is None:
            #print("looking for target group...")
            modname = self.targetmodule.get('name', None)
            modid = self.get_module_id(modname)
            #print("modname = %s, modid = %s" % (modname, modid))
            if not(modname in self.grouplookup):
                self.grouplookup[modname] = {}
            if not(gname in self.grouplookup[modname]):
                self.grouplookup[modname][gname] = self.lookup_group(modname, gname)
            self.owner['id'] = self.grouplookup[modname][gname]
        #else:
        #    print("already got owner id: %s" % self.owner.get('id', None))
        return self.owner['id']

    def get_targetuser_id(self):
        """ Get target user id, or None if not a user event. """
        if self.get_ownertype() != 'user':
            return None
        return self._target_userid()

    def _target_userid(self):
        """ Get target user id by lookup from name. If USERS_AS_GROUPS is set, event
            owner will be user's group rather than user, so only set owner id if that
            is not set.
        """
        uname = self.owner.get('name', None)
        if self.owner.get('type', None) in ('group', 'course') or uname is None:
            return None
        userid = self.owner.get('id', None)
        if USERS_AS_GROUPS or userid is None:
            if not(uname in self.userlookup):
                self.userlookup[uname] = self.lookup_user(uname)
            userid = self.userlookup[uname]
            if not USERS_AS_GROUPS:
                self.owner['id'] = userid
        return userid

    def create_group(self):
        """ Create group (not implemented) """
        self.errors.append('Group creation not implemented! (for "%s")' % self.owner['name'])
        return None

    def get_ownertype(self):
        """ Get owner type of event (site/course/group/student).
            Careful not to recurse!
        """
        # if known already, return it
        if 'type' in self.owner:
            return self.owner['type']
        # unknowable
        if self.owner.get('name', None) is None:
            return None
        # if user exists & can be mapped, type is 'user'
        if self._target_userid() is not None and not USERS_AS_GROUPS:
            #print("Owner type is 'user'")
            self.owner['type'] = 'user'
        # if group exists & can be mapped, type is 'group'
        elif self._target_groupid() is not None:
            #print("Owner type is 'group'")
            self.owner['type'] = 'group'
        elif self._target_userid() is not None and USERS_AS_GROUPS and CREATE_USERGROUPS:
            # create group, set type to group
            #print("Owner type would be 'user' but will be 'group'")
            self.create_group()
            self.owner['type'] = 'group'
        # fail if there was something to find, and we couldn't find it
        elif self.owner.get('name', None) not in ('', None):
            #print("Owner type is 'none'")            
            self.owner['type'] = None
            unfound = "user/group"
            if USERS_AS_GROUPS:
                unfound = "group"
            self.errors.append('Failed to find event owner (%s) "%s"' % (unfound, self.owner['name']))
        # else type is 'course' - we don't want to create site events this way at the moment
        # maybe look at leaving ModuleUpload_Shortname blank *and* setting Student-Group_Name to 'site'
        # if this is desired?
        else:
            self.owner['type'] = 'course'
        return self.owner['type']

    def get_moodle_eventtype(self):
        """ Don't recall why we have this as it just returns result of get_ownertype """
        return self.get_ownertype()

    def get_alm_text(self):
        """ Get text to use for ALM calendar event """
        descdict = {
            'title': self.title,
            'staff': self.staff,
            'location': self.location['full'],
            'contact': self.contactdept,
            'comments': self.comments,
            'activity': self.get_activity_link(),
            'module': self.get_module_link()
        }
        description = """
%(title)s<br>
%(staff)s<br>
%(location)s<br>
%(contact)s<br>
%(comments)s<br>
%(activity)s<br>
%(module)s
""" % descdict
        return description

    def get_calentry(self):
        """ Get a dict representing this calendar entry, suitable for upload
            via JSON webservice.
        """
        # moodle won't accept nulls for courseid, groupid, so we must either
        # set to 0 or leave out of submission. 0 is more obvious/explicit, so...
        groupid = self.get_targetgroup_id()
        if groupid is None:
            groupid = 0
        courseid = self.get_targetmodule_id()
        if courseid is None:
            courseid = 0

        locationpart = ''
        if self.location['short'] is not None:
            locationpart = ': %s' % self.location['short']
            
        # No, we don't know what 'sequence' is. Neither does calendar source.
        return {
            'name': "%s: %s: %s%s" % (self.usereventtype,
                                        self.linkmodule['text'],
                                        self.owner['name'],
                                        locationpart),
            'description': self.get_alm_text(),
            'format': FORMAT_HTML,
            'courseid': courseid,
            'groupid': groupid,
            # currently not able to be specified at Moodle end
            #'userid': self.get_targetuser_id(),
            'repeats': 0,
            'eventtype': self.get_moodle_eventtype(), # 'site', 'group', 'course', 'user'
            'timestart': self.startdt,
            'timeduration': self.duration,
            'visible': 1,
            'sequence': 1
        }

    def dump(self):
        """ Dump the event object for debugging """
        print("EVENT:")
        print("title: %s" % self.title)
        print("owner: %s" % self.owner['name'])
        print("courseid: %s" % self.get_targetmodule_id())
        print("groupid: %s" % self.get_targetgroup_id())
        print("type: %s" % self.get_moodle_eventtype())
        return True
        
    @classmethod
    def from_row(cls, row):
        """ Create an event object from a spreadsheet row """
        isodate = row.get('isodate', '')
        olddate = row.get('Date', '')
        if (isodate == '' and olddate == ''):
            return None
        event = cls()
        event._find_title(row)
        event._find_usereventtype(row)
        event._find_owner(row)
        event._find_start(row)
        event._find_duration(row)
        event._find_activity(row)
        event._find_linkmodule(row)
        event._find_targetmodule(row)
        event._find_staff(row)
        event._find_location(row)
        event._find_contactdept(row)
        event._find_comments(row)
        #event.dump()
        return event

with open(infile, newline='') if infile != '-' else sys.stdin as csvfile:
    reader = csv.DictReader(csvfile)
    fields_checked = False
    for row in reader:
        if not fields_checked:
            missing_fields = required_fields - {alternative_fields.get(f, False) or f for f in reader.fieldnames}
            if missing_fields:
                errors += ['Missing required field: "%s"' % f for f in missing_fields]
            missing_fields = optional_fields - {alternative_fields.get(f, False) or f for f in reader.fieldnames}
            if missing_fields:
                warnings += ['Missing optional field: "%s"' % f for f in missing_fields]
            fields_checked = True
        # dump documentation row(s) below headers...
        if row['Event_Name'] in ('id_description', 'event title'):
            continue

        event = Event.from_row(row)
        if event is not None:
            eventlist.append(event)

errors += Event.lookup_modules()
errors += Event.lookup_users()

events['events'] = list(map(lambda e: e.get_calentry(), eventlist))

course_event_count = len([e for e in events['events'] if e['groupid'] == 0])
if course_event_count > 0:
    warnings.append("%s COURSE events present" % course_event_count)

log.warning(pprint.pformat(events))
sys.stdout.write('dry run is %s\n' % conf.get_item('dryrun'))

warnings.extend(reduce(lambda a, v: chain(a, v.warnings), eventlist, []))
if warnings:
    log.warning("WARNINGS:\n" + pprint.pformat(warnings) + "\n")

errors.extend(reduce(lambda a, v: chain(a, v.errors), eventlist, []))
if errors:
    log.critical("ERRORS:\n" + pprint.pformat(errors) + "\n")
    sys.exit(1)

if conf.get_item('dryrun'):
    sys.exit(0)

# post to API
# use requests.post

url = ''.join(
    [
        moodlews,
        '?wstoken=',
        moodletoken,
        '&wsfunction=core_calendar_create_calendar_events',
        '&moodlewsrestformat=json'
    ]
)
response = requests.post(url, json=events)
print("STATUS %s\n" % response.status_code)
if response.status_code != 200:
    print("FAILED\n")

print(response.content)
