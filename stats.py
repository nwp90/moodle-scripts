#!/usr/bin/env python3

import datetime
import pprint

import muddle

class StatsAppConfig(muddle.AppConfig):
    def add_args(self):
        self.argparser.add_argument('-C', '--course', default=None, help='Retrieve stats for course with this shortname.')

conf = StatsAppConfig.get()
conf.cli("Test use of muddle API for stats retrieval")

mstatsapi = muddle.stats.API(conf.m)
foo = mstatsapi.daily_activity_by_shortname(conf.get_item('course'))
pprint.pprint(foo)
