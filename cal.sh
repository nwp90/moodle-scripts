#!/bin/bash

SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )
# no realpath on macos...
#pypath=$(dirname $(realpath $0))
echo $SCRIPTPATH

PYTHONPATH=$SCRIPTPATH/muddle:$PYTHONPATH $SCRIPTPATH/calendarupload.py $*
