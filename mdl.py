#!/usr/bin/env python3

import pprint
import json
import argparse
import psycopg2
import psycopg2.extras
import os
import sys
import collections
import csv
import time

global DEBUG
DEBUG=False


class UserError(RuntimeError):
    """Exception raised when we detect a user error (e.g. in config)

    Attributes:
        message -- explanation of error
    """
    pass


class Moodle(object):
    # Moodle context levels (*sigh*...)
    # CONTEXT_SYSTEM 		the whole site 		10
    # CONTEXT_USER 		another user 		30
    # CONTEXT_COURSECAT 	a course category 	40
    # CONTEXT_COURSE 		a course 		50
    # CONTEXT_MODULE 		an activity module 	70
    # CONTEXT_BLOCK 		a block 		80
    #
    # See https://docs.moodle.org/dev/Roles#Context
    # This determines what the instanceid in mdl_context is
    # a Foreign Key to...
    #
    contextdata = [
        ("SITE",   "Site", 10),
        ("USER",   "User", 30),
        ("CAT",    "Course Category", 40),
        ("COURSE", "Course", 50),
        ("MOD",    "Module", 70),
        ("BLOCK",  "Block", 80),
        ]

    def __init__(self):
        self.ctxname = {}
        self.ctxlevel = {}
        for tpl in self.contextdata:
            setattr(self, 'CTX_' + tpl[0], tpl[2])
            self.ctxname[tpl[2]] = tpl[1]
            self.ctxlevel[tpl[0]] = tpl[2]


class Reporter(object):
    _reports = [
        'reports',
        'files',
        'filecheck',
        'rolecsv',
        'roles',
        'assessments',
        'courses',
        'coursestats',
        'courseformats',
        'cohorts',
        'blockusage',
        'qdetails',
        'tagged'
        ]

    def default_report(self):
        self.config.error("Report '%s' not available\n" % self.config.get_item('report'))

    def __init__(self, config):
        self.config = config
        report = config.get_item('report')
        if report in self._reports:
            self.report = getattr(self, report, self.default_report)
        else:
            self.report = self.default_report

    def get_dbconn(self):
        conn = getattr(self, 'conn', None)
        if conn is not None:
            return conn
        site = self.config.get_site()
        self.conn = psycopg2.connect(
            database=site['dbname'],
            user=site['dbuser'],
            password=site['dbpass'],
            host=site['dbhost'],
            port=site['dbport'],
            sslmode="require"
            )
        return self.conn

    def reports(self):
        for k in sorted(self._reports):
            print(k)

    def files(self):
        pattern = self.config.get_item('pattern')
        if pattern != '':
            files = find_file_usage(self.get_dbconn(), pattern)
        else:
            files = find_file_usage(self.get_dbconn())

        for filehash, filelist in files.items():
            print("\n%s:" % filehash)
            for fileinstance in filelist:
                if fileinstance['refid']:
                    fileinstance['refindicator'] = "* "
                else:
                    fileinstance['refindicator'] = ""
                print("%(filename)s %(refindicator)s/ %(author)s / ctx inst id %(ctxinstanceid)s / compt %(component)s / filearea %(filearea)s / filepath %(filepath)s" % fileinstance)
                print("\t%(containertype)s \"%(containername)s\"\t(%(contextstring)s)" % fileinstance)
            print()

    def filecheck(self):
        site = self.config.get_site()
        repo_path = site.get('repo_path', None)
        data_path = site.get('data_path', None)
        if repo_path is None:
            self.config.error("repo_path not available for site '%s'\n" % self.config.get_item('site'))
        if data_path is None:
            self.config.error("data_path not available for site '%s'\n" % self.config.get_item('site'))

        pattern = self.config.get_item('pattern')
        if pattern != '':
            files = find_file_usage(self.get_dbconn(), pattern)
        else:
            files = find_file_usage(self.get_dbconn())
            
        for filehash, filelist in files.items():
            errs = False
            if not os.path.isfile(os.path.join(data_path, "filedir", filehash[0:2], filehash[2:4], filehash)):
                heading = "\n***\nMISSING %s:" % filehash
                errs = True
            elif filehash == EMPTY_HASH:
                heading = "\n***\nEMPTY: %s:" % filehash
            else:
                heading = "\n***\n%s:" % filehash

            for fileinstance in filelist:
                if fileinstance['containername'] is None:
                    errs = True

            if errs:
                print(heading)
                for fileinstance in filelist:
                    if fileinstance['filename'] == '.' and fileinstance['containername'] is not None:
                        continue
                    if fileinstance['containername'] is None:
                        print("ORPHANED file? CM has no module instance id")
                    print("%(filename)s / %(author)s / ctx inst id %(ctxinstanceid)s / compt %(component)s / filearea %(filearea)s / filepath %(filepath)s" % fileinstance)
                    print("\t%(containertype)s \"%(containername)s\"\t(%(contextstring)s)" % fileinstance)
                    print()

# FYI - to check whether a thing is referenced from a course, check mdl_course_sections.sequence,
# which is comma-sep list of mdl_course_modules ids.
#
# This will include all resources, scorms etc. But that's only one of the context levels.

        
    def roles(self):
        pattern = self.config.get_item('pattern')
        if pattern != '':
            roles = find_roles(self.get_dbconn(), pattern)
        else:
            roles = find_roles(self.get_dbconn())

        for roleshort, roleassignments in roles.items():
            print("\n%s:" % roleshort)
            for user in roleassignments:
                print("%(firstname)s %(lastname)s / %(username)s / %(email)s" % user)
                print("\t%(containertype)s \"%(containername)s\"\t(%(contextstring)s)\n" % user)
            print()

    def rolecsv(self):
        pattern = self.config.get_item('pattern')
        if pattern != '':
            roles = find_roles(self.get_dbconn(), pattern)
        else:
            roles = find_roles(self.get_dbconn())

        for roleshort, roleassignments in roles.items():
            for user in roleassignments:
                print("%(username)s\t%(firstname)s\t%(lastname)s\t%(email)s\t%(city)s\t%(containertype)s\t%(containername)s\t%(ctxinstanceid)s" % user)
            print()

    def courses(self):
        courses = find_courses(self.get_dbconn())
        writer = csv.writer(sys.stdout)
        if courses:
            writer.writerow(sorted(courses[0].keys()))
        for course in courses:
            writer.writerow([str(course[k]) for k in sorted(course.keys())])

    def courseformats(self):
        pattern = self.config.get_item('pattern')
        if pattern:
            courses = find_courses(self.get_dbconn(), format=pattern)
        else:
            courses = find_courses(self.get_dbconn())

        formats = collections.defaultdict(list)
        for course in courses:
            formats[course['format']].append(course)
        for fname, fdata in formats.items():
            print("\n\nFormat '%s':\n" % fname)
            for course in fdata:
                print("%(shortname)s\t(%(id)s):\t\t%(fullname)s" % course)

    def assessments(self):
        cfg = Configuration.get()
        if cfg.get_item('cmatch') is not None:
            courses = [dict(c) for c in find_courses(self.get_dbconn())]
            for course in courses:
                course['assessments'] = get_assessments(self.get_dbconn(), courseid=course['id'])
            writer = csv.writer(sys.stdout)
            if courses:
                columns = sorted(courses[0]['assessments'][0].keys())
                writer.writerow(columns)
            for course in courses:
                for row in course['assessments']:
                    data = [str(row[k]) for k in sorted(row.keys())]
                    writer.writerow(data)
        else:
            stats = get_assessments(self.get_dbconn(), pattern=cfg.get_item('pattern'))
            writer = csv.writer(sys.stdout)
            writer.writerow(sorted(stats[0].keys()))
            for row in stats:
                writer.writerow([str(row[k]) for k in sorted(row.keys())])

    def coursestats(self):
        cfg = Configuration.get()
        if cfg.get_item('cmatch') is not None:
            courses = [dict(c) for c in find_courses(self.get_dbconn())]
            for course in courses:
                course['stats'] = get_stats(self.get_dbconn(), courseid=course['id'])
            writer = csv.writer(sys.stdout)
            if courses:
                columns = sorted(courses[0]['stats'][0].keys())
                writer.writerow(columns)
            for course in courses:
                for row in course['stats']:
                    data = [str(row[k]) for k in sorted(row.keys())]
                    writer.writerow(data)
        else:
            stats = get_stats(self.get_dbconn(), pattern=cfg.get_item('pattern'))
            writer = csv.writer(sys.stdout)
            writer.writerow(sorted(stats[0].keys()))
            for row in stats:
                writer.writerow([str(row[k]) for k in sorted(row.keys())])

    def cohorts(self):
        cohorts = find_cohorts(self.get_dbconn())
        for cohort in cohorts:
            print("Cohort '%(name)s' %(visibility)s, %(membercnt)s members, held in context '%(contexttype)s %(contextname)s':" % cohort)
            if cohort['courses']:
                for course in cohort['courses']:
                    print("\tenrolled in '%(shortname)s' (%(id)s), %(statustext)s" % course)
            else:
                print("\tnot enrolled in any course")
            print()

    def blockusage(self):
        blocks = get_block_instances(self.get_dbconn())
        for blockname, blocklist in blocks.items():
            print("\n%s:" % blockname)
            for block in blocklist:
                print("\t%(containertype)s %(containername)s\t(%(contextstring)s)" % block)
            print()

    def qdetails(self):
        cfg = Configuration.get()
        pattern = self.config.get_item('pattern')
        # temporary(ish); add idmatch option similar to -C later?
        idlist = []
        if pattern != '':
            idlist.append(pattern)
        if idlist:
            for qid in idlist:
                question = get_question_details(self.get_dbconn(), qid)
                print("QUESTION ID %s:\n%s\n\n" % (question['id'], pprint.pformat(question)))
        else:
            self.config.error("Please specify question id to detail with pattern arg.")

    def tagged(self):
        cfg = Configuration.get()
        pattern = self.config.get_item('pattern')        
        if pattern != '':
            tags = find_tagged(self.get_dbconn(), pattern)
        else:
            tags = find_tagged(self.get_dbconn())

        for tag, things in tags.items():
            print("\n%s:" % tag)
            for thing in things:
                print("\t%(itemtype)s: %(thingname)s / %(useremail)s / course id %(courseid)s" % thing)
            print()
            
        
class Configuration(object):
    defaults = {
        'debug': False,
        'config': os.path.join(os.path.expanduser('~'), '.mdl'),
        'report': None,
        'site': None,
        'sites': {},
        'pattern': '',
        }

    def __init__(self):
        argparser = argparse.ArgumentParser(description='Grab information from Moodle DB')
        argparser.add_argument('-c', '--config', default=self.defaults['config'], help='path to JSON config file')
        argparser.add_argument('-s', '--site', default=None, help='name of site to access')
        argparser.add_argument('-r', '--report', default=None, help='name of report to produce, "reports" for list of valid options')
        argparser.add_argument('-p', '--pattern', default=None, help='SQL pattern to match records against (details depend on report)')
        argparser.add_argument('-C', '--cmatch', action='append', default=None, help='restrict output to records associated with course(s) matched by SQL pattern specified (on short and full names). Specify multiple times to require multiple matches.')
        argparser.add_argument('-d', '--debug', action='store_true', default=None, help='debug mode')
        self.argparser = argparser
        self.args = argparser.parse_args()

    @classmethod
    def get(cls):
        if getattr(cls, '__config', None) is None:
            cls.__config = cls()
        return cls.__config

    def error(self, msg):
        sys.stderr.write(u'ERROR: %s\n' % msg)
        self.argparser.print_help()
        sys.exit(1)

    def get_item(self, name):
        if name != 'config':
            if getattr(self, 'conf', None) is None:
                self.read_json_config()
            conf = self.conf
        else:
            conf = {}
        if name in ['sites', 'services']:
            key = name
        else:
            key = 'default_' + name
        var = conf.get(key, None)
        if var is None:
            var = self.defaults.get(name, None)
        if getattr(self.args, name, None) is not None:
            var = getattr(self.args, name, None)
        return var

    def read_json_config(self):
        conffile = open(self.get_item('config'))
        self.conf = json.load(conffile)
        conffile.close()
        return self.conf

    def merge_site_config(self):
        site_config = self.get_site()

    def get_site(self):
        site = self.get_item('site')
        sites = self.get_item('sites')
        if site is None:
            self.error(u'No site specified')
        site_config = sites.get(site, None)
        if site_config is None:
            self.error(u"No config available for site '%s'" % site)
        return site_config

def get_module_instance_name(cursor, modulename, instanceid):
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']

    if DEBUG: sys.stderr.write("getting name for module instance %s %s\n" % (modulename, instanceid))
    tablename = "%s_%s" % (prefix, modulename)
    sql = "SELECT name FROM %s WHERE id = %%s" % tablename
    cursor.execute(sql, [instanceid])
    rows = cursor.fetchall()
    if rows:
        if rows[0]['name'] is None:
            return ''
        if DEBUG: sys.stderr.write("got name '%s' for module instance %s %s\n" % (rows[0]['name'], modulename, instanceid))
        return rows[0]['name']
    if DEBUG: sys.stderr.write("couldn't find module instance with id %s\n" % instanceid)
    return None

def get_contexts(cursor, contextids):
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    contexts = {}
    for contextid in contextids:
        if DEBUG: sys.stderr.write("contextid = %s\n" % contextid)
        if not (contextid in contexts):
            sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.contextlevel contextlevel FROM %(prefix)s_context WHERE id = %%s" % { 'prefix': prefix }
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            contextlevel = rows[0]['contextlevel']
            sql = None
            if contextlevel == 10:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'site' containertype, 'system' containername FROM %(prefix)s_context WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 30:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'user' containertype, %(prefix)s_user.username containername FROM %(prefix)s_context JOIN %(prefix)s_user ON %(prefix)s_context.instanceid = %(prefix)s_user.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 40:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'course category' containertype, %(prefix)s_course_categories.name containername FROM %(prefix)s_context JOIN %(prefix)s_course_categories ON %(prefix)s_context.instanceid = %(prefix)s_course_categories.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 50:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'course' containertype, %(prefix)s_course.shortname containername FROM %(prefix)s_context JOIN %(prefix)s_course ON %(prefix)s_context.instanceid = %(prefix)s_course.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 70:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'module' containertype, %(prefix)s_modules.name containername FROM %(prefix)s_context JOIN %(prefix)s_course_modules ON %(prefix)s_context.instanceid = %(prefix)s_course_modules.id JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if sql is None:
                continue
            if DEBUG: sys.stderr.write("sql = %s\ncontextid = %s\n" % (sql, contextid))
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            row = rows[0]
            if DEBUG: sys.stderr.write("contextid=%s, rowctxid=%s\n" % (contextid, row['contextid']))
            assert(str(row['contextid']) == contextid)
            if DEBUG: sys.stderr.write("adding to contexts: id %(contextid)s type %(containertype)s name %(containername)s\n" % row)
            contexts[str(row['contextid'])] = {
                'id': row['contextid'],
                'type': row['containertype'],
                'name': row['containername'],
                'instanceid': row['instanceid'],
            }
    return contexts

gradetypes = {
    0: 'NONE',
    1: 'NUMBER',
    2: 'SCALE',
    3: 'TEXT'
}

def get_assessments(conn, courseid=None, pattern=None):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    basesql = """
               SELECT gi.courseid, c.shortname AS courseshortname, gi.categoryid, gc.parent AS categoryparent,
                 gc.fullname AS categoryname, gi.itemname AS gradename, gi.itemtype AS gradeitemtype,
                 gi.itemmodule AS grademodule, gi.idnumber AS gradeidnumber, gi.gradetype,
                 gi.scaleid, (sc.courseid = 0) AS scaleglobal, sc.name AS scalename, gi.sortorder
               FROM %(prefix)s_grade_items gi
               JOIN %(prefix)s_course c ON gi.courseid = c.id
               LEFT JOIN %(prefix)s_grade_categories gc ON gi.categoryid = gc.id
               LEFT JOIN %(prefix)s_scale sc ON gi.scaleid = sc.id
              """ % { 'prefix': prefix }
    orderby = "ORDER BY gi.courseid, gi.sortorder"
    sqlparams = []
    if courseid is not None:
        whereclause = "WHERE gi.courseid = %s"
        sqlparams.append(courseid)
    elif pattern is not None:
        whereclause = "WHERE c.shortname ILIKE %s"
        sqlparams.append(pattern)
    else:
        return []
    sql = ' '.join((basesql, whereclause, orderby))
    if DEBUG: sys.stderr.write("query is: %s" % sql)
    cursor.execute(sql, sqlparams)
    assessments = [ dict(c) for c in cursor.fetchall() ]
    for assess in assessments:
        assess['gradetype'] = gradetypes[assess['gradetype']]
    return assessments

def get_stats(conn, courseid=None, pattern=None):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    basesql = "SELECT c.shortname AS courseshortname, sm.courseid, sm.roleid, r.shortname AS roleshortname, r.name AS rolename, sm.timeend, sum(sm.stat1 + sm.stat2) AS activity FROM %(prefix)s_stats_monthly AS sm JOIN %(prefix)s_role AS r ON (sm.roleid = r.id) JOIN %(prefix)s_course AS c ON (sm.courseid = c.id)" % { 'prefix': prefix }
    groupby = " GROUP BY sm.timeend, sm.roleid, r.shortname, r.name, c.id, c.shortname, sm.courseid ORDER BY sm.courseid, sm.roleid, sm.timeend"
    sqlparams = []
    if courseid is not None:
        whereclause = "WHERE sm.stattype='activity' AND sm.courseid = %s"
        sqlparams.append(courseid)
    elif pattern is not None:
        whereclause = "WHERE sm.stattype='activity' AND c.shortname ILIKE %s"
        sqlparams.append(pattern)
    else:
        return []
    sql = ' '.join((basesql, whereclause, groupby))
    if DEBUG: sys.stderr.write("query is: %s" % sql)
    cursor.execute(sql, sqlparams)
    stats = [ dict(c) for c in cursor.fetchall() ]
    for s in stats:
        ltime = time.localtime(s['timeend'] - 1)
        s['month'] = '%04d%02d' % ltime[0:2] # year, month
    return stats

def find_cohorts(conn):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    pattern = cfg.get_item('pattern')
    basesql = "SELECT %(prefix)s_cohort.*, count( %(prefix)s_cohort_members.userid ) as membercnt FROM %(prefix)s_cohort JOIN %(prefix)s_cohort_members ON %(prefix)s_cohort.id = %(prefix)s_cohort_members.cohortid" % { 'prefix': prefix }
    groupby = "GROUP BY %(prefix)s_cohort.id" % { 'prefix': prefix }
    whereclause = ""
    sqlparams = []
    if pattern != '':
        whereclause = "WHERE %(prefix)s_cohort.name ILIKE %%s" % { 'prefix': prefix }
        sqlparams.append(pattern)
    sql = ' '.join((basesql, whereclause, groupby))
    if DEBUG: sys.stderr.write("sql = %s\nparams = %s\n" % (sql, pprint.pformat(sqlparams)))
    cursor.execute(sql, sqlparams)
    cohorts = [ dict(c) for c in cursor.fetchall() ]
    needcontexts = set()
    statustext = {
        0: 'active',
        1: 'inactive',
        }
    vistext = {
        0: 'not visible',
        1: 'visible',
        }
    for cohort in cohorts:
        if DEBUG: sys.stderr.write("cohort: %s\n" % pprint.pformat(cohort))
        # status = 1 => inactive, 0 => active
        sql = "SELECT %(prefix)s_course.id, %(prefix)s_course.fullname,  %(prefix)s_course.shortname,  %(prefix)s_course.visible, %(prefix)s_enrol.status, %(prefix)s_enrol.enrolstartdate, %(prefix)s_enrol.enrolenddate FROM %(prefix)s_course JOIN %(prefix)s_enrol ON %(prefix)s_enrol.courseid = %(prefix)s_course.id WHERE %(prefix)s_enrol.enrol = 'cohort' AND %(prefix)s_enrol.customint1 = %%s" % { 'prefix': prefix }
        sqlparams = [cohort['id']]
        if DEBUG: sys.stderr.write("sql = %s\nparams = %s\n" % (sql, pprint.pformat(sqlparams)))
        cursor.execute(sql, sqlparams)
        cohort['courses'] = [ dict(c) for c in cursor.fetchall() ]
        for course in cohort['courses']:
            course['statustext'] = statustext[course['status']]
        cohort['visibility'] = vistext[cohort['visible']]
        cohort['contextid'] = str(cohort['contextid'])
        needcontexts.add(cohort['contextid'])
    contexts = get_contexts(cursor, needcontexts)
    for cohort in cohorts:
        cohort.update({
            'contextname': contexts[cohort['contextid']]['name'],
            'contexttype': contexts[cohort['contextid']]['type'],
            })
    return cohorts

def find_courses(conn, **kwargs):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    coursematch = cfg.get_item('cmatch')
    sql = "SELECT * FROM %(prefix)s_course WHERE TRUE" % { 'prefix': prefix }
    sqlparams = []
    if coursematch is not None:
        for term in coursematch:
            sql = sql + " AND (%(prefix)s_course.shortname ILIKE %%s)" % { 'prefix': prefix }
            if DEBUG: sys.stderr.write("adding term to coursematch: %s" % term)
            sqlparams.append(term)
    for column, pattern in kwargs.items():
        data = {
            'prefix': prefix,
            'column': column,
            'pattern': pattern,
        }
        sql = sql + " AND (%(prefix)s_course.%(column)s ILIKE %%s)" % data
        if DEBUG: sys.stderr.write("adding term to pattern match: %(column)s ~ %(pattern)s" % data)
        sqlparams.append(pattern)

    if DEBUG: sys.stderr.write("sql = %s\nparams = %s\n" % (sql, pprint.pformat(sqlparams)))
    cursor.execute(sql, sqlparams)
    return cursor.fetchall()


def find_roles(conn, rolematch=None):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    coursematch = cfg.get_item('cmatch')

    roles = collections.defaultdict(list)
    gotcontexts = set()
    needcontexts = set()
    contexts = {}
    mdl = Moodle()

    for (contextlevel, contextname) in mdl.ctxname.items():
        commonselect = "SELECT %(prefix)s_role.id roleid, %(prefix)s_role.name rolename, %(prefix)s_role.shortname roleshort, %(prefix)s_role_assignments.id id, roleuser.firstname, roleuser.lastname, roleuser.username, roleuser.email, roleuser.city, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, "
        commonfrom = "FROM %(prefix)s_role JOIN %(prefix)s_role_assignments ON (%(prefix)s_role.id = %(prefix)s_role_assignments.roleid) JOIN %(prefix)s_user roleuser ON (%(prefix)s_role_assignments.userid = roleuser.id) JOIN %(prefix)s_context ON (%(prefix)s_role_assignments.contextid = %(prefix)s_context.id) "
        sql = None
        if coursematch is None:
            if contextlevel == mdl.CTX_SITE:
                sql = (commonselect + "'site' containertype, 'system context' containername " + commonfrom + "WHERE contextlevel = 10") % { 'prefix': prefix }
            if contextlevel == mdl.CTX_USER:
                sql = (commonselect + "'user' containertype, %(prefix)s_user.username containername " + commonfrom + "JOIN %(prefix)s_user ON (contextlevel = 30 AND %(prefix)s_context.instanceid = %(prefix)s_user.id)") % { 'prefix': prefix }
            if contextlevel == mdl.CTX_CAT:
                sql = (commonselect + "'course category' containertype, %(prefix)s_course_categories.name containername " + commonfrom + "JOIN %(prefix)s_course_categories ON (contextlevel = 40 AND %(prefix)s_context.instanceid = %(prefix)s_course_categories.id)") % { 'prefix': prefix }
        if contextlevel == mdl.CTX_COURSE:
            sql = (commonselect + "'course' containertype, %(prefix)s_course.shortname containername " + commonfrom + "JOIN %(prefix)s_course ON (contextlevel = 50 AND %(prefix)s_context.instanceid = %(prefix)s_course.id)") % { 'prefix': prefix }
        if contextlevel == mdl.CTX_MOD:
            sql = (commonselect + "CONCAT(%(prefix)s_modules.name, ' module') containertype, %(prefix)s_modules.name containername, %(prefix)s_course_modules.instance modinstanceid, %(prefix)s_course.shortname " + commonfrom + "JOIN %(prefix)s_course_modules ON (contextlevel = 70 and %(prefix)s_context.instanceid = %(prefix)s_course_modules.id) JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module JOIN %(prefix)s_course ON %(prefix)s_course_modules.course = %(prefix)s_course.id") % { 'prefix': prefix }
        if sql is None:
            continue

        sqlparams = []
        if coursematch is not None:
            for term in coursematch:
                sql = sql + " AND (%(prefix)s_course.shortname ILIKE %%s)" % { 'prefix': prefix }
                if DEBUG: sys.stderr.write("adding term to coursematch: %s" % term)
                sqlparams.append(term)
        if rolematch is not None:
            sql = sql + " AND ( %(prefix)s_role.name ILIKE %%s or %(prefix)s_role.shortname ILIKE %%s)" % { 'prefix': prefix }
            sqlparams.extend([rolematch, rolematch])

        if DEBUG: sys.stderr.write("sql is: %s\nrolematch is: %s\ncoursematch is: %s\n" % (sql, rolematch, pprint.pformat(coursematch)))
        if sqlparams:
            cursor.execute(sql, sqlparams)
        else:
            cursor.execute(sql)

        rows = cursor.fetchall()

        for row in rows:
            containertype = row['containertype']
            containername = row['containername']
            contextid = row['contextid']
            contextinstanceid = row['instanceid']
            if contextlevel == 70:
                if DEBUG: sys.stderr.write("getting module instance name for %s, row: %s\n" % (containername, pprint.pformat(row)))
                containername = get_module_instance_name(cursor, containername, row['modinstanceid'])
            rowcontexts = row['contextpath'].split('/')[1:]
            roles[row['roleshort']].append({
                'id': row['id'],
                'contextpath': rowcontexts,
                'containertype': containertype,
                'containername': containername,
                'ctxinstanceid': contextinstanceid,
                'firstname': row['firstname'],
                'lastname': row['lastname'],
                'username': row['username'],
                'email': row['email'],
                'city': row['city'],
            })
            contexts[contextid] = {
                'id': contextid,
                'type': containertype,
                'name': containername,
                'instanceid': contextinstanceid,
            }
            if DEBUG: sys.stderr.write(pprint.pformat(row.keys()))
            if DEBUG: sys.stderr.write("\ncontexts for role assignment %s: %s\n" % (row['id'], '/'.join(set(rowcontexts))))
            needcontexts = needcontexts | set(rowcontexts)
            gotcontexts.add(contextid)

    contexts.update(get_contexts(cursor, needcontexts - gotcontexts))

    for rolelist in roles.values():
        for roleinstance in rolelist:
            if DEBUG: sys.stderr.write("got contextpath %s\n" % ','.join(fileinstance['contextpath']))
            roleinstance['contextstring'] = ' => '.join(["%(type)s %(name)s (%(id)s)" % contexts[cid] for cid in roleinstance['contextpath']])

    return roles

def find_file_usage(conn, filematch=None):
    # currently undecided whether filematch/coursematch should be passed in or
    # grabbed from config
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    coursematch = cfg.get_item('cmatch')

    files = collections.defaultdict(list)
    gotcontexts = set()
    needcontexts = set()
    contexts = {}
    mdl = Moodle()
    for (contextlevel, contextname) in mdl.ctxname.items():
        if DEBUG: sys.stderr.write("Contextlevel %s (%s)\n" % (contextlevel, contextname))
        commonselect = "SELECT %(prefix)s_files.author, %(prefix)s_files.contenthash, %(prefix)s_files.pathnamehash, %(prefix)s_files.filename, %(prefix)s_files.component, %(prefix)s_files.filearea, %(prefix)s_files.filepath, %(prefix)s_files.referencefileid, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, "
        commonfrom = "FROM %(prefix)s_files JOIN %(prefix)s_context ON %(prefix)s_files.contextid = %(prefix)s_context.id "
        sql = None
        if coursematch is None:
            if contextlevel == mdl.CTX_SITE:
                sql = (commonselect + "'site' containertype, 'system context' containername " + commonfrom + "WHERE contextlevel = 10") % { 'prefix': prefix }
            if contextlevel == mdl.CTX_USER:
                sql = (commonselect + "'user' containertype, %(prefix)s_user.username containername " + commonfrom + "JOIN %(prefix)s_user ON (contextlevel = 30 AND %(prefix)s_context.instanceid = %(prefix)s_user.id)") % { 'prefix': prefix }
            if contextlevel == mdl.CTX_CAT:
                sql = (commonselect + "'course category' containertype, %(prefix)s_course_categories.name containername " + commonfrom + "JOIN %(prefix)s_course_categories ON (contextlevel = 40 AND %(prefix)s_context.instanceid = %(prefix)s_course_categories.id)") % { 'prefix': prefix }
        if contextlevel == mdl.CTX_COURSE:
            sql = (commonselect + "'course' containertype, %(prefix)s_course.shortname containername " + commonfrom + "JOIN %(prefix)s_course ON (contextlevel = 50 AND %(prefix)s_context.instanceid = %(prefix)s_course.id)") % { 'prefix': prefix }
        if contextlevel == mdl.CTX_MOD:
            sql = (commonselect + "CONCAT(%(prefix)s_modules.name, ' module') containertype, %(prefix)s_modules.name containername, %(prefix)s_course_modules.instance modinstanceid, %(prefix)s_course.shortname " + commonfrom + "JOIN %(prefix)s_course_modules ON (contextlevel = 70 and %(prefix)s_context.instanceid = %(prefix)s_course_modules.id) JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module JOIN %(prefix)s_course ON %(prefix)s_course_modules.course = %(prefix)s_course.id") % { 'prefix': prefix }
        if sql is None:
            continue

        sqlparams = []
        if coursematch is not None:
            for term in coursematch:
                sql = sql + " AND (%(prefix)s_course.shortname ILIKE %%s)" % { 'prefix': prefix }
                if DEBUG: sys.stderr.write("adding term to coursematch: %s" % term)
                sqlparams.append(term)
        if filematch is not None:
            sql = sql + " AND (%(prefix)s_files.filename ILIKE %%s)" % { 'prefix': prefix }
            sqlparams.append(filematch)

        if DEBUG: sys.stderr.write("sql is: %s\nfilematch is: %s\ncoursematch is: %s\n" % (sql, filematch, pprint.pformat(coursematch)))
        if sqlparams:
            cursor.execute(sql, sqlparams)
        else:
            cursor.execute(sql)

        rows = cursor.fetchall()

        for row in rows:
            containertype = row['containertype']
            containername = row['containername']
            contextid = row['contextid']
            contextinstanceid = row['instanceid']
            if contextlevel == 70:
                if DEBUG: sys.stderr.write("getting module instance name for %s, row: %s\n" % (containername, pprint.pformat(row)))
                containername = get_module_instance_name(cursor, containername, row['modinstanceid'])
            rowcontexts = row['contextpath'].split('/')[1:]
            files[row['contenthash']].append({
                'contextpath': rowcontexts,
                'containertype': containertype,
                'containername': containername,
                'ctxinstanceid': contextinstanceid,
                'refid': row['referencefileid'],
                'component': row['component'],
                'filearea': row['filearea'],
                'filepath': row['filepath'],
                'filename': row['filename'],
                'pathnamehash': row['pathnamehash'],
                'author': row['author'],
            })
            contexts[contextid] = {
                'id': contextid,
                'type': containertype,
                'name': containername,
                'instanceid': contextinstanceid,
            }
            if DEBUG: sys.stderr.write(pprint.pformat(row.keys()))
            if DEBUG: sys.stderr.write("\ncontexts for file %s: %s\n" % (row['filename'], '/'.join(set(rowcontexts))))
            needcontexts = needcontexts | set(rowcontexts)
            gotcontexts = gotcontexts | set([contextid])

    contexts.update(get_contexts(cursor, needcontexts - gotcontexts))
    
    for filelist in files.values():
        for fileinstance in filelist:
            if DEBUG: sys.stderr.write("got contextpath %s\n" % ','.join(fileinstance['contextpath']))
            fileinstance['contextstring'] = ' => '.join(["%(type)s %(name)s (%(id)s)" % contexts[cid] for cid in fileinstance['contextpath']])

    return files


def get_block_instances(conn, blockname=None):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    blocks = collections.defaultdict(list)
    needcontexts = set()
    contexts = {}
    mdl = Moodle()

    for (contextlevel, contextname) in mdl.ctxname.items():
        sql = None
        if contextlevel == 10:
            sql = "SELECT %(prefix)s_block_instances.blockname blockname, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'site' containertype, 'system context' containername FROM %(prefix)s_block_instances JOIN %(prefix)s_context ON %(prefix)s_block_instances.parentcontextid = %(prefix)s_context.id WHERE contextlevel = 10"
        if contextlevel == 30:
            sql = "SELECT %(prefix)s_block_instances.blockname blockname, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'user' containertype, %(prefix)s_user.username containername FROM %(prefix)s_block_instances JOIN %(prefix)s_context ON %(prefix)s_block_instances.parentcontextid = %(prefix)s_context.id JOIN %(prefix)s_user ON (contextlevel = 30 AND %(prefix)s_context.instanceid = %(prefix)s_user.id)"
        if contextlevel == 40:
            sql = "SELECT %(prefix)s_block_instances.blockname blockname, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'course category' containertype, %(prefix)s_course_categories.name containername FROM %(prefix)s_block_instances JOIN %(prefix)s_context ON %(prefix)s_block_instances.parentcontextid = %(prefix)s_context.id JOIN %(prefix)s_course_categories ON (contextlevel = 40 AND %(prefix)s_context.instanceid = %(prefix)s_course_categories.id)"
        if contextlevel == 50:
            sql = "SELECT %(prefix)s_block_instances.blockname blockname, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'course' containertype, %(prefix)s_course.shortname containername FROM %(prefix)s_block_instances JOIN %(prefix)s_context ON %(prefix)s_block_instances.parentcontextid = %(prefix)s_context.id JOIN %(prefix)s_course ON (contextlevel = 50 AND %(prefix)s_context.instanceid = %(prefix)s_course.id)"
        if contextlevel == 70:
            sql = "SELECT %(prefix)s_block_instances.blockname blockname, %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, CONCAT(%(prefix)s_modules.name, ' module') containertype, %(prefix)s_modules.name containername, %(prefix)s_course_modules.instance instanceid FROM %(prefix)s_block_instances JOIN %(prefix)s_context ON %(prefix)s_block_instances.parentcontextid = %(prefix)s_context.id JOIN %(prefix)s_course_modules ON (contextlevel = 70 and %(prefix)s_context.instanceid = %(prefix)s_course_modules.id) JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module"
        if sql is None:
            continue
        cursor.execute(sql % { 'prefix': prefix })
        rows = cursor.fetchall()

        for row in rows:
            containertype = row['containertype']
            containername = row['containername']
            contextid = row['contextid']
            if contextlevel == 70:
                containername = '"' + get_module_instance_name(cursor, containername, row['instanceid']) + '"'
            rowcontexts = row['contextpath'].split('/')[1:]
            blocks[row['blockname']].append({
                'contextpath': rowcontexts,
                'containertype': containertype,
                'containername': containername,
            })
            contexts[contextid] = {
                'id': contextid,
                'type': containertype,
                'name': containername,
            }
            if DEBUG: stderr.write("contexts for block %s: %s\n" % (row['blockname'], '/'.join(set(rowcontexts))))
            needcontexts = needcontexts | set(rowcontexts)

    for contextid in needcontexts:
        if DEBUG: stderr.write("contextid = %s\n" % contextid)
        if not (contextid in contexts):
            sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.contextlevel contextlevel FROM %(prefix)s_context WHERE id = %%s" % { 'prefix': prefix }
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            contextlevel = rows[0]['contextlevel']
            sql = None
            if contextlevel == 10:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'site' containertype, 'system' containername, 0 containerid FROM %(prefix)s_context WHERE %(prefix)s_context.id = %%s"
            if contextlevel == 30:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'user' containertype, %(prefix)s_user.username containername, %(prefix)s_user.id containerid FROM %(prefix)s_context JOIN %(prefix)s_user ON %(prefix)s_context.instanceid = %(prefix)s_user.id WHERE %(prefix)s_context.id = %%s"
            if contextlevel == 40:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'course category' containertype, %(prefix)s_course_categories.name containername, %(prefix)s_course_categories.id containerid FROM %(prefix)s_context JOIN %(prefix)s_course_categories ON %(prefix)s_context.instanceid = %(prefix)s_course_categories.id WHERE %(prefix)s_context.id = %%s"
            if contextlevel == 50:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'course' containertype, %(prefix)s_course.shortname containername, %(prefix)s_course.id containerid FROM %(prefix)s_context JOIN %(prefix)s_course ON %(prefix)s_context.instanceid = %(prefix)s_course.id WHERE %(prefix)s_context.id = %%s"
            if contextlevel == 70:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, 'module' containertype, %(prefix)s_modules.name containername, %(prefix)s_modules.id containerid FROM %(prefix)s_context JOIN %(prefix)s_course_modules ON %(prefix)s_context.instanceid = %(prefix)s_course_modules.id JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module WHERE %(prefix)s_context.id = %%s"
            if sql is None:
                continue
            sql = sql % { 'prefix': prefix }
            if DEBUG: stderr.write("sql = %s\ncontextid = %s\n" % (sql, contextid))
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            row = rows[0]
            if DEBUG: stderr.write("contextid=%s, rowctxid=%s\n" % (contextid, row['contextid']))
            assert(str(row['contextid']) == contextid)
            if DEBUG: stderr.write("adding to contexts: ctxid %(contextid)s type %(containertype)s name %(containername)s id %(containerid)s\n" % row)
            contexts[str(row['contextid'])] = {
                'id': row['contextid'],
                'cid': row['containerid'],
                'type': row['containertype'],
                'name': row['containername'],
            }
    
    for blocklist in blocks.values():
        for block in blocklist:
            if DEBUG: stderr.write("got contextpath %s\n" % ','.join(block['contextpath']))
            block['contextstring'] = ' => '.join(["%(type)s %(name)s (%(id)s/%(cid)s)" % contexts[cid] for cid in block['contextpath']])

    return blocks

def get_question_details(conn, qid):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']

    questionquery = "SELECT q.id, q.category, q.parent, q.name, q.questiontext, q.qtype, q.stamp, q.version, q.timecreated, q.timemodified FROM %(prefix)s_question q WHERE id = %%s" % { 'prefix': prefix }
    quizslotquery = "SELECT id, quizid, slot FROM %(prefix)s_quiz_slots WHERE questionid = %%s" % { 'prefix': prefix }
    quizquery = "SELECT q.course, q.name, c.fullname, c.shortname FROM %(prefix)s_quiz q JOIN %(prefix)s_course c ON q.course = c.id WHERE q.id = %%s" % { 'prefix': prefix }
    categoryquery = "SELECT c.id, c.name, c.info, c.contextid, ctx.contextlevel, ctx.instanceid, ctx.path FROM %(prefix)s_question_categories c JOIN %(prefix)s_context ctx ON c.contextid = ctx.id WHERE c.id = %%s" % { 'prefix': prefix }
    subqquery = "SELECT q.id, q.name FROM %(prefix)s_question q WHERE parent = %%s" % { 'prefix': prefix }
    aquery = "SELECT a.id, a.answer, a.fraction FROM %(prefix)s_question_answers a WHERE question = %%s" % { 'prefix': prefix }
    attquery = "SELECT count(*) AS attempts FROM %(prefix)s_question_attempts WHERE questionid = %%s" % { 'prefix': prefix }
    calcquery = "SELECT id, answer FROM %(prefix)s_question_calculated WHERE question = %%s" % { 'prefix': prefix }
    calcoptquery = "SELECT id FROM %(prefix)s_question_calculated_options WHERE question = %%s" % { 'prefix': prefix }
    dsetquery = "SELECT id, datasetdefinition FROM %(prefix)s_question_datasets WHERE question = %%s" % { 'prefix': prefix }
    ddwtosquery = "SELECT id FROM %(prefix)s_question_ddwtos WHERE questionid = %%s" % { 'prefix': prefix }
    gapfillquery = "SELECT id FROM %(prefix)s_question_gapfill WHERE question = %%s" % { 'prefix': prefix }
    gapselectquery = "SELECT id FROM %(prefix)s_question_gapselect WHERE questionid = %%s" % { 'prefix': prefix }
    hintsquery = "SELECT id, hint FROM %(prefix)s_question_hints WHERE questionid = %%s" % { 'prefix': prefix }
    maquery = "SELECT id, sequence FROM %(prefix)s_question_multianswer WHERE question = %%s" % { 'prefix': prefix }
    numquery = "SELECT id, answer, tolerance FROM %(prefix)s_question_numerical WHERE question = %%s" % { 'prefix': prefix }
    oumrquery = "SELECT id, answernumbering FROM %(prefix)s_question_oumultiresponse WHERE questionid = %%s" % { 'prefix': prefix }
    qraquery = "SELECT id, hashcode, response FROM %(prefix)s_question_response_analysis WHERE questionid = %%s" % { 'prefix': prefix }
    statsquery = "SELECT id, hashcode, slot, subquestion FROM %(prefix)s_question_statistics WHERE questionid = %%s" % { 'prefix': prefix }
    tfquery = "SELECT id, trueanswer, falseanswer FROM %(prefix)s_question_truefalse WHERE question = %%s" % { 'prefix': prefix }

    cursor.execute(questionquery, [qid])
    rows = cursor.fetchall()
    rawq = rows[0]
    question = {
        'id':        rawq['id'],
        'catid':     rawq['category'],
        'parentid':  rawq['parent'],
        'name':      rawq['name'],
        'text':      rawq['questiontext'],
        'type':      rawq['qtype'],
        'stamp':     rawq['stamp'],
        'version':   rawq['version'],
        'ctime':     rawq['timecreated'],
        'mtime':     rawq['timemodified']
        }

    cursor.execute(quizslotquery, [qid])
    rows = cursor.fetchall()
    quizzes = {}
    for row in rows:
        quizzes[row['quizid']] = {
            'slot': row['slot']
            }
    for quizid, quiz in quizzes.items():
        cursor.execute(quizquery, [quizid])
        rows = cursor.fetchall()
        row = rows[0]
        quiz.update({
            'name': row['name'],
            'courseid': row['course'],
            'coursefull': row['fullname'],
            'courseshort': row['shortname']
            })
    question['quizzes'] = quizzes

    cursor.execute(categoryquery, [question['catid']])
    rows = cursor.fetchall()
    category = rows[0]
    question.update({
        'catname':   category['name'],
        'catinfo':   category['info'],
        'catctxid':  category['contextid'],
        'catctxlevel': category['contextlevel'],
        'catctxinstid': category['instanceid'],
        'catctxpath': category['path']
        })

    subqs = {}
    cursor.execute(subqquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        subqs[row['id']] = row['name']
    question['subqs'] = subqs

    answers = {}
    cursor.execute(aquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        answers[row['id']] = {
            'answer':   row['answer'],
            'fraction': row['fraction']
            }
    question['answers'] = answers

    cursor.execute(attquery, [qid])
    rows = cursor.fetchall()
    question['attempts'] = rows[0]['attempts']

    calculated = {}
    cursor.execute(calcquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        calculated[row['id']] = {
            'answer':   row['answer']
            }
    question['calculated'] = calculated

    calcopt = []
    cursor.execute(calcoptquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        calcopt.append(row['id'])
    question['calculated_options'] = calcopt

    datasets = {}
    cursor.execute(dsetquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        datasets[row['id']] = {
            'datasetdefinition': row['datasetdefinition']
            }
    question['datasets'] = datasets

    ddwtos = []
    cursor.execute(ddwtosquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        ddwtos.append(row['id'])
    question['ddwtos'] = ddwtos

    gapfill = []
    cursor.execute(gapfillquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        gapfill.append(row['id'])
    question['gapfill'] = gapfill

    gapselect = []
    cursor.execute(gapselectquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        gapselect.append(row['id'])
    question['gapselect'] = gapselect

    hints = {}
    cursor.execute(hintsquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        hints[row['id']] = {
            'hint': row['hint']
            }
    question['hints'] = hints

    multianswer = {}
    cursor.execute(maquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        multianswer[row['id']] = {
            'sequence': row['sequence']
            }
    question['multianswer'] = multianswer

    numerical = {}
    cursor.execute(numquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        numerical[row['id']] = {
            'answer': row['answer'],
            'tolerance': row['tolerance']
            }
    question['numerical'] = numerical

    oumultiresponse = {}
    cursor.execute(oumrquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        oumultiresponse[row['id']] = {
            'answernumbering': row['answernumbering']
            }
    question['oumultiresponse'] = oumultiresponse

    response_analysis = {}
    cursor.execute(qraquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        response_analysis[row['id']] = {
            'hashcode': row['hashcode'],
            'response': row['response']
            }
    question['response_analysis'] = response_analysis

    statistics = {}
    cursor.execute(statsquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        statistics[row['id']] = {
            'hashcode':    row['hashcode'],
            'slot':        row['slot'],
            'subquestion': row['subquestion']
            }
    question['statistics'] = statistics

    truefalse = {}
    cursor.execute(tfquery, [qid])
    rows = cursor.fetchall()
    for row in rows:
        truefalse[row['id']] = {
            'trueanswer':  row['trueanswer'],
            'falseanswer': row['falseanswer']
            }
    question['truefalse'] = truefalse


    
    needcontexts = set([str(question['catctxid'])]) | set(question['catctxpath'].split('/')[1:])

    contexts = {}
    for contextid in needcontexts:
        if DEBUG: stderr.write("contextid = %s\n" % contextid)
        if not (contextid in contexts):
            sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.contextlevel contextlevel FROM %(prefix)s_context WHERE id = %%s" % { 'prefix': prefix }
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            contextlevel = rows[0]['contextlevel']
            sql = None
            if contextlevel == 10:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'site' containertype, 'system' containername FROM %(prefix)s_context WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 30:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'user' containertype, %(prefix)s_user.username containername FROM %(prefix)s_context JOIN %(prefix)s_user ON %(prefix)s_context.instanceid = %(prefix)s_user.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 40:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'course category' containertype, %(prefix)s_course_categories.name containername FROM %(prefix)s_context JOIN %(prefix)s_course_categories ON %(prefix)s_context.instanceid = %(prefix)s_course_categories.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 50:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'course' containertype, %(prefix)s_course.shortname containername FROM %(prefix)s_context JOIN %(prefix)s_course ON %(prefix)s_context.instanceid = %(prefix)s_course.id WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if contextlevel == 70:
                sql = "SELECT %(prefix)s_context.id contextid, %(prefix)s_context.path contextpath, %(prefix)s_context.contextlevel, %(prefix)s_context.instanceid, 'module' containertype, %(prefix)s_modules.name containername FROM %(prefix)s_context JOIN %(prefix)s_course_modules ON %(prefix)s_context.instanceid = %(prefix)s_course_modules.id JOIN %(prefix)s_modules ON %(prefix)s_modules.id = %(prefix)s_course_modules.module WHERE %(prefix)s_context.id = %%s" % { 'prefix': prefix }
            if sql is None:
                continue
            if DEBUG: stderr.write("sql = %s\ncontextid = %s\n" % (sql, contextid))
            cursor.execute(sql, [contextid])
            rows = cursor.fetchall()
            row = rows[0]
            if DEBUG: stderr.write("contextid=%s, rowctxid=%s\n" % (contextid, row['contextid']))
            assert(str(row['contextid']) == contextid)
            if DEBUG: stderr.write("adding to contexts: id %(contextid)s type %(containertype)s name %(containername)s\n" % row)
            contexts[str(row['contextid'])] = {
                'id': row['contextid'],
                'type': row['containertype'],
                'name': row['containername'],
                'instanceid': row['instanceid'],
            }
    question['contexts'] = contexts
    return question

# currently this only understands tagged resources; needs to understand all possible things
# that could be tagged.
#
def find_tagged(conn, tagname=None):
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cfg = Configuration.get()
    prefix = cfg.get_site()['prefix']
    tags = collections.defaultdict(list)

    tagquerybase = "SELECT t.id, t.name, t.rawname, tc.name AS tagtype, i.id AS instid, i.itemtype, mm.name AS modname, cm.instance AS modinstid, i.itemid, u.username, u.firstname, u.lastname, u.email FROM %(prefix)s_tag t JOIN %(prefix)s_tag_coll tc ON t.tagcollid = tc.id JOIN %(prefix)s_tag_instance i ON t.id = i.tagid LEFT JOIN %(prefix)s_user u on i.tiuserid = u.id LEFT JOIN (%(prefix)s_course_modules cm JOIN %(prefix)s_modules mm ON mm.id = cm.module) ON (i.itemtype = 'course_modules' AND i.itemid = cm.id) " % { 'prefix': prefix }
    resourcequery = "SELECT r.id, r.course, r.name FROM %(prefix)s_resource r WHERE r.id = %%s" % { 'prefix': prefix }

    if tagname is None:
        cursor.execute(tagquerybase)
    else:
        tagquery = tagquerybase + ' WHERE t.name ILIKE %s'
        cursor.execute(tagquery, [tagname])
    rows = cursor.fetchall()
    for row in rows:
        if DEBUG: print("handling tag row...")
        resid = None
        # Old tagging
        if row['itemtype'] == 'resource':
            resid = row['itemid']
        # New tagging
        if row['modname'] == 'resource':
            resid = row['modinstid']
        if resid is not None:
            tag = {
                'id':        row['id'],
                'name':      row['name'],
                'rawname':   row['rawname'],
                'type':      row['tagtype'],
                'instid':    row['instid'],
                'itemtype':  row['itemtype'] if row['itemtype'] != 'course_modules' else row['modname'],
                'itemid':    row['itemid'] if row['itemtype'] != 'course_modules' else row['modinstid'],
                'username':  row.get('username', None),
                'userfirst': row.get('userfirst', None),
                'userlast':  row.get('userlast', None),
                'useremail': row.get('useremail', None)
                }
            if DEBUG: pprint.pprint(tag)
            cursor.execute(resourcequery, [resid])
            resources = list(cursor.fetchall())
            resource = resources[0]
            tag.update({
                'thingid':   resource['id'],
                'thingname': resource['name'],
                'courseid':  resource['course']
                })
            tags[row['name']].append(tag)
    return tags


def main():
    global DEBUG
    config = Configuration.get()
    DEBUG = config.get_item('debug')
    Reporter(config).report()


if __name__ == "__main__":
    main()
