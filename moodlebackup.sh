#!/bin/bash

set -e

error () {
    echo "ERROR: $1"
    exit 1
}

getconfig () {
    SETTING=$1

    grep -m1 "^\$CFG->${SETTING}" ${CONFFILE}| cut -d= -f2 | cut -d\' -f2
}

SITENAME=$1
DATE=$(date "+%Y%m%d%H%M")
CONFFILE="/etc/moodle-site/${SITENAME}/config.php"
BACKUPDIR="./moodle-backup-${DATE}"
BACKUPTARFILE="./moodle-backup-${DATE}.tar"
SITEDIR="/srv/moodle/${SITENAME}"
LOGFILE="./moodle-backup-log-${DATE}"

[ -e ${CONFFILE} ] || error "No config file for site '${SITENAME}'"
[ -e ${BACKUPDIR} ] && error "Can't create backup dir '${BACKUPDIR}: already exists"
mkdir ${BACKUPDIR} || error "mkdir failed to create backup dir"
chmod 700 ${BACKUPDIR}
SQLFILE="moodle-db-backup.sql"
#CONTENTFILE=${BACKUPDIR}/moodle-content-backup.tbz

DBHOST=$(getconfig "dbhost")
DBNAME=$(getconfig "dbname")
DBUSER=$(getconfig "dbuser")
DBPASS=$(getconfig "dbpass")

echo "DBHOST=${DBHOST}"
echo "DBNAME=${DBNAME}"
echo "DBUSER=${DBUSER}"
echo "DBPASS=${DBPASS}"

#echo "See ${CONFFILE} for pg password!"

PGSSLMODE=require pg_dump -n public -U ${DBUSER} -d ${DBNAME} -h ${DBHOST} -i --file ${BACKUPDIR}/${SQLFILE} 2>&1 | tee ${LOGFILE}

[ -e ${BACKUPTARFILE} ] && error "Can't create backup file '${BACKUPTARFILE}: already exists"
tar -C ${BACKUPDIR} -cvf ${BACKUPTARFILE} ${SQLFILE} 2>&1 | tee -a ${LOGFILE}

sudo tar -C ${SITEDIR} -rvf ${BACKUPTARFILE}  --exclude=data/[0-9]*/backupdata/* data 2>&1 | tee -a ${LOGFILE}

bzip2 -9 ${BACKUPTARFILE} 2>&1 | tee -a ${LOGFILE}
#rm -r ${BACKUPDIR}
