#!/usr/bin/env python3

# Filter Moodle XML format questions by tag.
# Use as filter in shell pipeline or redirection e.g.
#
# tagfilter.py MCQ < downloadedfile.xml > newfile.xml
#
# to filter downloadedfile.xml for questions tagged 'MCQ' and dump results
# into newfile.xml
#
# BEWARE: tags are case-sensitive
#

import sys
import re
import xml.etree.ElementTree as xET

try:
    import defusedxml.cElementTree as dET
except ImportError:
    try:
        import defusedxml.ElementTree as dET
    except ImportError:
        sys.stderr.write("WARNING: couldn't import defusedxml ElementTree, using vulnerable version.\n See https://docs.python.org/3/library/xml.html#xml-vulnerabilities\n")
        import xml.etree.ElementTree as dET

if len(sys.argv) > 1:
    filtertag = sys.argv[1]
else:
    sys.stderr.write("Error: filter tag must be provided\n")
    sys.exit(1)
    
if not (re.match('^\w+$', filtertag)):
    sys.stderr.write("Error: filter tag must be one or more 'word characters'\n")
    sys.exit(1)

inroot = dET.parse(sys.stdin).getroot()
outroot = xET.Element('quiz')

for child in inroot:
    if child.tag == 'question' and child.attrib['type'] != 'category':
        if child.find("./tags/tag/[text='%s']" % filtertag):
            outroot.append(child)

outtree = xET.ElementTree(element=outroot)
outtree.write(sys.stdout, encoding='unicode', short_empty_elements=False)
