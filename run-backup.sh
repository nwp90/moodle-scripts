#!/bin/sh

sudo -u www-data /bin/sh -c "/usr/bin/aexec -t 4320 -f /run/lock/moodle-backup.lock -d 0 /usr/bin/nice -n19 /usr/bin/ionice -c3 -t /usr/bin/php /srv/moodle/otagounifom/moodle/admin/cli/automated_backups.php >> /var/log/sitelogs/moodle-site-otagounifom/cli_backup.log 2>&1"
